/**
 * 
 */
package payroll;


import payrollExceptions.DataNumberMismatchException;
import payrollExceptions.InvalidDataException;

/**
 * @author edward
 *
 */
public class FileReader {

	private FileIO fileIOObj;
	private int taxID;
	private Name name;
	private EmploymentType type;
	private double rate;
	private double ytd;
	private Date dates;
	private double hours;
	private double deduction;

	public FileReader (FileIO fileIOObj) {
		this.fileIOObj = fileIOObj;
	}

	/**
	 * Construct a Employee by reading a line from the file.
	 * Each call reads a new line and returns a new employee.
	 * @return Employee
	 */
	public AbstractEmployee getAEmployee() {
		assert fileIOObj.getStatus() : "File not yet open.";
		String readString = fileIOObj.readLineFromFile();
		if(readString == null) {
			return null;
		}
		parseArgumentsInLine(readString.split("\t"));
		if(this.type == EmploymentType.HOURLY) {
			return new HourlyEmployee(this.taxID, this.name, this.rate, this.ytd, 
					this.dates, this.hours, this.deduction);
		}
		else {
			return new SalariedEmployee(this.taxID, this.name, this.rate, this.ytd, 
					this.dates, this.hours, this.deduction);
		}
	}

	/**
	 * Private method to parse all the necessary information about for a Employee,
	 * and set the relevant field of this class.
	 * @param String[] splitString
	 */
	private void parseArgumentsInLine(String[] splitString) {
		if(splitString.length != 9) {
			// If the columns of data is not 9
			throw new DataNumberMismatchException();
		}
		int i = 0;
		for(InputType inType : InputType.values()) {
			// Iterate through the enum InputType to parse the data correctly
			try {
				parser(inType , splitString[i++]);
			} catch (Exception ex) {
				//Catches InvalidDataException and NumberFormateException
				System.err.println(ex);
				System.exit(1);
			}
		}


	}

	/**
	 * Private method to parse individual String.
	 * @param inType
	 * @param toParse
	 * @throws NumberFormatException
	 * @throws InvalidDataException
	 */
	private void parser(InputType inType, String toParse) 
			throws NumberFormatException, InvalidDataException {
		switch(inType) {
		case TID:
			this.taxID = Integer.parseUnsignedInt(toParse); // Can throw NumberFormate Exception
			break;
		case NAME:
			this.name = new Name(toParse.split(",")); // Split the name
			break;
		case EMPLOYMENT:
			if(toParse.matches("Salaried")) {
				this.type = EmploymentType.SALARIED;
			}
			else if(toParse.matches("Hourly")) {
				this.type = EmploymentType.HOURLY;
				//TODO is this acceptable?
			}
			else {
				// Not "Salaried" nor "Hourly"
				throw new InvalidDataException(toParse 
						+ " does not match any employment type.");
			}
			break;
		case RATE:
			this.rate = this.parseMoney(toParse);
			break;
		case YTD:
			this.ytd = this.parseMoney(toParse);
			break;
		case START:
			this.dates = new Date(parseDate(toParse));
			break;
		case END:
			this.dates.setEndDate(parseDate(toParse));
			break;
		case HOURS:
			this.hours = Double.parseDouble(toParse);
			break;
		case DEDUCTION:
			this.deduction = this.parseMoney(toParse);
			break;
		default:
			assert false : toParse;
		}
	}

	/**
	 * Private method to parse money value.
	 * @param toParse The String to be parsed
	 * @return sum The money sum in double
	 * @throws NumberFormatException, InvalidDataException
	 */
	private double parseMoney(String toParse) throws NumberFormatException, InvalidDataException {
		if(toParse.matches("\\$[0-9]+\\.[0-9]+")) {
			double doubleTemp = Double.parseDouble(toParse.substring(1)); // Removes the dollar sign
			if(doubleTemp < 0.0) {
				// Checks for negative values as we are parsing a signed value
				throw new InvalidDataException(doubleTemp + " is not a valid value.");
			}
			return doubleTemp;
		}
		else if(toParse.matches("\\$[0-9]+")) {
			return Integer.parseUnsignedInt(toParse.substring(1)); // Can throw NumFormat Exception, removes the dollar sign
		}
		throw new InvalidDataException("Invalid money value: " + toParse);

	}

	/**
	 * Private method to parse a date String.
	 * @param toParse String to be parsed
	 * @return dates A 3 by 1 Array of integer containing the date value. YYYY MM DD
	 * @throws NumberFormatException, InvalidDataException
	 */
	private int[] parseDate(String toParse) throws NumberFormatException, InvalidDataException {

		String[] items;
		items = toParse.split("[-]"); // Split into individual Strings
		if(items.length != 3) {
			// Something is wrong
			throw new InvalidDataException("Invalid date value: " + toParse);
		}
		int[] temp = new int[3];
		for(int index = 0; index < items.length; index++) {
			temp[index] = Integer.parseUnsignedInt(items[index]);  // Can throw NumFormat Exception.
		}
		return temp;
	}

}
