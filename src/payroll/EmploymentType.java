/**
 * 
 */
package payroll;

/**
 * @author edward
 *
 */
public enum EmploymentType {
	
	SALARIED, HOURLY

}
