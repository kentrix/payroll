/**
 * 
 */
package payroll;

/**
 * @author edward
 *
 */
public class Date {
	
	private int[] start;
	private int[] end;
	
	/**
	 * Construct a Date object with 6 integers as argument
	 * @param startYear
	 * @param startMonth
	 * @param startDay
	 * @param endYear
	 * @param endMonth
	 * @param endDay
	 */
	public Date(int startYear, int startMonth, int startDay,
				int endYear, int endMonth, int endDay) {
		this.start = new int[]{startYear, startMonth, startDay};
		this.end = new int[]{endYear, endMonth, endDay};
	}
	
	/**
	 * Construct a Data object with 2 array of integers, formated 
	 * int[]{YYYY, MM, DD};
	 * @param start
	 * @param end
	 */
	public Date(int[] start, int[] end) {
		this.start = start;
		this.end = end;
	}
	
	/**
	 * Construct a Data object with the start date only.
	 * formated int[]{YYYY, MM, DD}
	 * @param start
	 */
	public Date(int[] start) {
		this.start = start;
	}
	
	/**
	 * Set the end date of, to be used in conjunction with the constructor 
	 * which takes in only the start date.
	 * formated int[]{YYYY, MM, DD}
	 * @param start
	 * @see Date(int[] start)
	 */
	public void setEndDate(int[] end) {
		this.end = end;
	}
	
	/**
	 * Get the formatted version of the start date for output.
	 * @return formatted String
	 */
	public String getStartDateFormat() {
		return(String.format("%4d-%02d-%02d", start[0], start[1], start[2]));
	}

	/**
	 * Get the formatted version of the end date for output.
	 * @return formatted String
	 */
	public String getEndDateFormat() {
		return(String.format("%4d-%02d-%02d", end[0], end[1], end[2]));
	}

}
