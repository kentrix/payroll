/**
 * 
 */
package payroll;

/**
 * @author edward
 *
 */
public class SalariedEmployee extends AbstractEmployee{

	private double payAsYouEarn;	

	public SalariedEmployee(int taxID, Name name, double rate, 
			double ytd, Date dates, double hours, double deduction) {
		super(taxID, name, rate, ytd, dates, hours, deduction); // Calls the AbstactEmployee constructor.
		this.payAsYouEarn = calculatePAYE(rate); // Use the static method to calculate the PAYE.
	}

	/**
	 * Get the gross of an employee
	 * @return gross
	 */
	@Override
	public double getPayPerPeriod() {
		return getRate() / 52;
	}

	/**
	 * Get the PAYE of an employee.
	 * @return PAYE
	 */
	@Override
	double getPAYE() {
		return this.payAsYouEarn;
	}

	/**
	 * Get the Nett of an employee.
	 * @return nett
	 */
	@Override
	double getNett() {
		return this.getPayPerPeriod() - this.getPAYE() - getDeduction();
	}

}
