/**
 * 
 */
package payroll;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import payrollExceptions.EmployeeNotUniqueException;
import payrollExceptions.InvalidDataException;

/**
 * @author edward
 *
 */
public class EmployeeList implements Iterable<AbstractEmployee>{

	private List<AbstractEmployee> list = new ArrayList<>();
	private int index = 0;
	
	/**
	 * Construct a list of Employees with a FileReader object.
	 * The FileIO object of the FileReader object should be opened for reading.
	 * 
	 * @param fileReader A fileReader object
	 */
	public EmployeeList(FileReader file) {
		AbstractEmployee employee;
		while((employee = file.getAEmployee()) != null) {
			try {
				verify(employee); // Checks for errors in the data
			} catch (Exception ex) {
				ex.printStackTrace();
				System.exit(1);
			}
			list.add(employee);
		}
	}
	
	/**
	 * @deprecated
	 * @return
	 */
	public boolean hasNext() {
		if(list.get(index) != null) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * @deprecated
	 * @return
	 */
	public AbstractEmployee next() {
		return list.get(index++);
	}

	public Iterator<AbstractEmployee> iterator(){
		return list.iterator();
	}
	/**
	 * Returns the next Employee each time it's called.
	 * Returns null if already at the end of the list and wrap around.
	 * @return Employee
	 * @deprecated
	 */
	public AbstractEmployee getNextEmployee() {
		if(index < list.size()) {
			return list.get(index++);
		}
		else {
			index = 0;
			return null;
		}
	}
	/**
	 * Returns the pay period Date.
	 * Only returns the Date found in the first element of the list, assuming all 
	 * employees have the same period.
	 * @return Date 
	 * 
	 */
	public Date getPayPeriod() {
		assert list.size() >= 1;
		return list.get(0).getDates();
	}

	/**
	 * A private method to check the validity of the employee.
	 * Checks for duplicate TaxIDs and whether the same object appears more than once.
	 * @param employee
	 * @return validity
	 */
	private void verify(AbstractEmployee employee) throws EmployeeNotUniqueException, 
			InvalidDataException{
		if(list.contains(employee)) {
			throw new EmployeeNotUniqueException("Employee ID" +
					employee.getTaxID() + " is not unique.");
		}
		for(AbstractEmployee each : list) {
			if(each.getTaxID() == employee.getTaxID()) {
				throw new EmployeeNotUniqueException("Employee ID" +
						employee.getTaxID() + " is not unique.");
			}
		}
		if(employee.getDeduction() < 0.0) {
			throw new InvalidDataException("Employee ID:" + employee.getTaxID() +
											" has invalid deduction value: " + employee.getDeduction());
		}
		if(employee.getNett() < 0.0) {
			throw new InvalidDataException("Employee ID:" + employee.getTaxID() +
											" has invalid Nett value: " + employee.getNett());
		}
		if(employee.getYtd() < 0.0) {
			throw new InvalidDataException("Employee ID:" + employee.getTaxID() +
											" has invalid Ytd value: " + employee.getYtd());
		}
	}
	
	/**
	 * Sort the list of Employee by their TID, in increasing orders.
	 * @return none
	 * @param none
	 */
	public void sortByTID() {
		Collections.sort(list, new Comparator<AbstractEmployee>() {
	        @Override
	        public int compare(AbstractEmployee employee1, AbstractEmployee employee2)
	        {
	            return  (new Integer(employee1.getTaxID())).compareTo(employee2.getTaxID());
	        }
	    });
	}

	/**
	 * Sort the list of Employee by their last name, in alphabetical orders.
	 * @return none
	 * @param none
	 */
	public void sortByLastName() {
		Collections.sort(list, new Comparator<AbstractEmployee>() {
	        @Override
	        public int compare(AbstractEmployee employee1, AbstractEmployee employee2)
	        {
	            return  employee1.getName().getLastName().compareTo(employee2.getName().getLastName());
	        }
	    });
	}
	
}
