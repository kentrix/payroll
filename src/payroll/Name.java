package payroll;

/**
 * 
 * @author edward
 *
 */
public class Name {
	
	private String firstName;
	private String lastName;
	
	/**
	 * Construct a Name object with a firstName and a lastName
	 * @param firstName
	 * @param lastName
	 */
	public Name(String firstName, String lastName) {
		// Trim the String to remove blank characters
		this.firstName = firstName.trim();
		this.lastName = lastName.trim();
	}

	/**
	 * Construct a Name object with a array of 2 Strings.
	 * Format String[]{lastName, firstName}
	 * @param names,String[0] = LastName String[1] = firstName 
	 */
	public Name(String[] names) {
		// Trim the String to remove blank characters
		this.lastName = names[0].trim();
		this.firstName = names[1].trim();
		
	}
	
	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}
	
}
