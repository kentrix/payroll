/**
 * 
 */
package payroll;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author edward
 *
 */
public class FileIO {

	private Path pathToFile;
	private BufferedReader reader;
	private boolean status = false;

	/**
	 * Construct a FileIO object with the path to a
	 * file, given as a String.
	 * @param fileName The path to the file, absolute or relative.
	 */
	public FileIO(String fileName) {
		try {
			this.pathToFile = Paths.get(fileName);
		}
		catch(InvalidPathException ipe) {
			ipe.printStackTrace();
			System.err.println(ipe);
			System.exit(1);
		}
	}
	
	
	/**
	 * @deprecated
	 * @param toFind
	 */
	public void findFile(String toFind) {
		if( !(Files.isReadable(pathToFile) && Files.isRegularFile(pathToFile) )) {
			System.err.println("Unable to find regular file or the file is not readable at" + pathToFile.toString());
			System.exit(1);
		}
	}
	
	/**
	 * Open the file for reading.    
	 * Must be done before any actual reading.
	 * Also sets the internal state to indicate that the file
	 * is open.
	 * @param none
	 * @return none
	 * 
	 * 
	 */
	public void openFile() {
		if(status) {
			// File already opened
			return;
		}
		try {
			reader = Files.newBufferedReader(this.pathToFile);
		}
		catch (IOException IOEx) {
			IOEx.printStackTrace();
			System.err.format("IOException:%s%n", IOEx);
			System.exit(1);
		}
		status = true;

	}
	
	/**
	 * Get the next file from the file, given that the line is not empty or start with "#" 
	 * 
	 * @return A String contains all the characters in the line
	 * @param none
	 */
	public String readLineFromFile() {
		//assert this.status : "File must be open first before reading.";
		if(this.status != true) {
			throw new RuntimeException("File must be opened first before reading.");
		}
		try {
			String buffer;
			while(true) {
				buffer = reader.readLine();
				if( buffer != null && !(buffer.equals("") || buffer.startsWith("#")) ) {
					// Filter out lines starts with "#"
					return buffer;
				}
				else if (buffer == null){
					// EOF reached
					return buffer;
				}
			}
		} catch (IOException IOEx) {
			IOEx.printStackTrace();
			System.err.println("Unable to read from the file.");
			System.exit(1);
		}
		return null;
	}
	
	/**
	 * Close the file. 
	 * @param none
	 * @return none
	 */
	public void closeFile() {
		if(!status) {
			// File is already closed
			return;
		}
		try {
			reader.close();
		} catch (IOException IOEx) {
			IOEx.printStackTrace();
			System.err.println("Unable to close the file.");
			System.exit(1);
		}
		status = false;
	}
	
	/**
	 * Check whether the file is current open.
	 * @return status Is the file open
	 */
	public boolean getStatus() {
		return this.status;
	}
}
