package payroll;

public abstract class AbstractEmployee {
	
	private int taxID;
	private Name name;
	private double rate;
	private double ytd;
	private Date dates;
	private double hours;
	private double deduction;

	public AbstractEmployee(int taxID, Name name,
			double rate, double ytd, Date dates, double hours,
			double deduction) {

		this.taxID = taxID;
		this.name = name;
		this.rate = rate;
		this.ytd = ytd;
		this.dates = dates;
		this.hours = hours;
		this.deduction = deduction;
		
	}

	/**
	 * Get the taxID of an employee.
	 * @return taxID
	 */
	public int getTaxID() {
		return taxID;
	}

	/**
	 * Get the name of an employee.
	 * @return name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * Get the rate of an employee.
	 * @return rate
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * Get the YTD of an employee.
	 * @return Ytd
	 */
	public double getYtd() {
		return ytd;
	}

	/**
	 * Get the pay period of an employee.
	 * @return dates
	 */
	public Date getDates() {
		return dates;
	}

	/**
	 * Get the hours worked by an employee.
	 * @return hours
	 */
	public double getHours() {
		return hours;
	}

	/**
	 * Get the deduction of an employee.
	 * @return deduction
	 */
	public double getDeduction() {
		return deduction;
	}
	
	/**
	 * A static method to calculate the total amount of tax for the 
	 * entire year.
	 * @param yearlyIncome
	 * @return taxDue
	 */
	public static double calculatePAYE(double yearlyIncome) {
		double totalTax = 0;
		if(yearlyIncome <= 14000.00) {
			totalTax = yearlyIncome * 0.105;
		}
		else if(yearlyIncome <= 48000.00) {
			totalTax = 1470 + (yearlyIncome - 14000.00) * 0.175;
		}
		else if(yearlyIncome <= 70000.00) {
			totalTax = 7420 + (yearlyIncome - 48000.00) * 0.3;
		}
		else {
			totalTax = 14020 + (yearlyIncome - 70000.00) * 0.33;
		}
		return totalTax / 52;
	}

	abstract double getPayPerPeriod();
	abstract double getPAYE();
	abstract double getNett();

}
