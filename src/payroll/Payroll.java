/**
 * 
 */
package payroll;

/**
 * @author edward
 * A simple payroll system
 */
public class Payroll {

	/**
	 * @param args
	 * @return none 
	 * @see 
	 * @author edward
	 */
	public static void main(String[] args) {

		CommandParser command = new CommandParser(args);
		// Creates a CommandParser Object which parses the arguments.
		FileIO file = new FileIO(command.getFileString());
		// Creates a FileIO object, for reading the file.
		file.openFile();
		// Opens the file for reading.
		FileReader reader = new FileReader(file);
		// Creates a new FileReader object, by parsing the lines in the file.
		EmployeeList employeeList = new EmployeeList(reader);
		// Creates a new EmployeeList, with the all the data parsed from the file
		Processer processer = new Processer(command.getProcessingString());
		// Creates a new Processer object, with the type of processing as a String.
		processer.printProcessedOutput(employeeList);
		// Prints the processed output given a EmployeeList.
		file.closeFile();
		// Finally, close the file.
	}

}
