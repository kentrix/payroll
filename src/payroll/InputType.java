package payroll;
/**
 * @author edward
 *
 */
public enum InputType {
	TID, NAME, EMPLOYMENT, RATE, YTD,
	START, END, HOURS, DEDUCTION
}
