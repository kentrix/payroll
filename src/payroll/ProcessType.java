/**
 * 
 */
package payroll;

/**
 * @author edward
 *
 */
public enum ProcessType {
	PAYSLIPS, EMPLOYEES, BURDEN, PAYE

}
