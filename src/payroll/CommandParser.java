/**
 * 
 */
package payroll;

import payrollExceptions.ArgumentNumberMismatchException;

/**
 * @author edward
 *
 */
public class CommandParser {

	private String fileString;
	private String processingString;
	
	/**
	 * Construct a CommandParse object to provide a interface for
	 * checking the correctness.
	 * @param commands
	 */
	public CommandParser(String[] commands) {
		if(commands.length != 2) {
			throw new ArgumentNumberMismatchException("Number of argument is " 
					+ commands.length + "\nPlease specify 2 arugments.");
		}
		this.fileString = commands[0];
		this.processingString = commands[1];
	}
	
	/** 
	 * Get the String that represents the filename
	 * @return fileName String
	 */
	public String getFileString() {
		return fileString;
	}
	
	/**
	 * Get the String that represents the processing type
	 * @return processingType String
	 */
	public String getProcessingString() {
		return processingString;
	}

}
