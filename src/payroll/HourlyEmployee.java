package payroll;
/**
 * 
 */

/**
 * @author edward
 *
 */
public class HourlyEmployee extends AbstractEmployee{

	private double payPerPeriod;
	private double payAsYouEarn;
	public HourlyEmployee(int taxID, Name name, double rate, 
			double ytd, Date dates, double hours, double deduction) {
		super(taxID, name, rate, ytd, dates, hours, deduction); // Calls the AbstactEmployee constructor.
		this.payPerPeriod = this.calculatePayPerPeriod(); // Calculates the gross
		this.payAsYouEarn = calculatePAYE(this.payPerPeriod * 52); // Use the static method to calculate the PAYE
	}
	
	/**
	 * A private method to calculate the gross for hourly paid employee.
	 * @return gross
	 */
	private double calculatePayPerPeriod() {
		// Employee is working at a hourly rate,
		// the yearly income is calculated for tax computation
		// later.
		double payPerPeriod = getRate() * getHours();
		if(getHours() > 40) {
			payPerPeriod += getRate() * (getHours() - 40) * 0.5; 
		}
		if(getHours() > 43) {
			payPerPeriod += getRate() * (getHours() - 43);
		}
		return payPerPeriod;
	}
	
	/**
	 * Get the PAYE of an employee.
	 * @return PAYE
	 */
	@Override
	public double getPAYE() {
		return this.payAsYouEarn;
	}

	/**
	 * Get the Nett of an employee.
	 * @return nett
	 */
	@Override
	public double getNett() {
		return this.payPerPeriod - this.getPAYE() - getDeduction();
	}

	/**
	 * Get the gross of an employee
	 * @return gross
	 */
	@Override
	public double getPayPerPeriod() {
		return this.payPerPeriod;
	}

}
